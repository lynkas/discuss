"""discuss URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from discuss import settings
from user.views import Signup,Login,changea
from user.views import personal,pv,nf,cg,update_profile
from django.conf.urls.static import static
from django.contrib.auth.views import LogoutView
urlpatterns = [
    path('admin/', admin.site.urls),
    # path('account/',include('user.urls')),
    path('user/logout/',LogoutView.as_view(),{'next_page': '/'}),
    # path('account/',include('django.contrib.auth.urls')),
    # path('moderate/',include('moderate.urls')),
    path('user/register/',Signup),
    path('user/login/',Login),
    path('tag/',include('post.tagurls')),
    path('',include('post.posturls')),
    path('api/',include('api.urls')),
    path('user/',personal),
    path('user/verify/',pv),
    path('user/change/',cg),
    path('user/settings/',update_profile),
    path('notification/',nf),
    path('user/changea/',changea),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
