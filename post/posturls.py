from django.urls import path
from . import views
urlpatterns=[
    path('',views.all_post),
    path('<int:id>/',views.spec_post),
]