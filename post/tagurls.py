from django.urls import path
from . import views

urlpatterns=[
    path('',views.fav_tag_post),
    path('<str:tag>/',views.tag_post),

    # path('post/'),#TODO
    # path('comment/',)#TODO
]