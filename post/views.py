from django.shortcuts import render
from .functions import *
from django.http import HttpResponseRedirect,HttpResponseForbidden,HttpResponseBadRequest,HttpResponseNotFound
from django.urls import reverse
from post.models import Post,Image,Comment
from user.models import Notification
import collections
from datetime import datetime
import operator
from sitesettings.models import Topic
# Create your views here.

'''
Post page, URL call. 
'''
def all_post(req):
    if req.method=='POST' :
        if not req.user.is_authenticated:
            return HttpResponseRedirect('/user/login/')
        else:
            content = req.POST.get('content')
            uncommentable = req.POST.get('uncommentable')
            tag = req.POST.get('tag')
            image = req.FILES.get('image')

            if uncommentable:
                uncommentable=True
            else:
                uncommentable = False

            if tag:
                tag=Tag.objects.filter(id__in=tag.split('.')[:-1])
            else:
                tag=[]

            p=Post(user=req.user,content=content,commentable=not uncommentable)
            p.save()
            p.tags.add(*tag)

            if image:
                img = Image(user=req.user, image=image)
                img.save()
                p.image.add(img)
            return HttpResponseRedirect('/'+str(p.id))

    hot={}
    for i in Post.objects.all():
        for j in i.tags.all():
            if j.title not in hot:
                hot[j.title]=1
            else:
                hot[j.title] += 1
    od = sorted(hot.items(), key=operator.itemgetter(1))
    hot = reversed(list(od)[-min(20,len(od)):])
    topics=Topic.objects.all()
    if len(topics)>3:
        topics=topics[:3]
    context={
        "posts":get_new_posts_page(get_page(req)),
        "user":req.user,
        "type":"all",
        'hot':hot,
        'topics':topics

    }
    return render(req,"main.html",context=context)

def spec_post(req,id):
    post= Post.objects.filter(pk=id).first()

    if req.method == 'POST':
        if not req.user.is_authenticated:
            return HttpResponseRedirect('/user/login/')
        else:
            c=Comment(content=req.POST.get('content'),user=req.user,post=post)
            c.save()
            post.comments.add(c)
            Notification.objects.get_or_create(user=post.user,post=post,action='pc')
            return HttpResponseRedirect('/'+str(id))

    if not post:
        return HttpResponseNotFound('404')
    context = {
        "post": post,
        "user": req.user,
    }

    return render(req, "post.html", context=context)

'''
Favorite tag post page, URL call
'''
def fav_tag_post(req):
    if not req.user.is_authenticated:
        return HttpResponseRedirect('/user/login/')
    q=req.GET.get('q')
    if q:
        t,c=Tag.objects.get_or_create(title=q)
        return HttpResponseRedirect('/tag/'+t.title)
    context={
        "posts":get_tags_posts_page(req.user.userext.tags.all(),get_page(req)),
        "user":req.user,
        "type":"subscribe_tag"
    }
    return render(req,"main.html",context)

'''
Specific tag post page, URL call.
tag: tag in string
'''
def tag_post(req,tag):
    t=None
    if req.user.is_authenticated:
        s=req.GET.get('s')
        t,_=Tag.objects.get_or_create(title=tag)
        if t and s=='s':
            if t not in req.user.userext.tags.all():
                req.user.userext.tags.add(t)
        elif t and s=='u':
            if t in req.user.userext.tags.all():
                req.user.userext.tags.remove(t)

    context = {
        "posts": get_tag_posts_page(tag, get_page(req)),
        "user": req.user,
        "tag":tag,
        "ti":t,
        "type": "tag"
    }
    return render(req, "main.html", context)