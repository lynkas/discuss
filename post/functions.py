from post.models import Post,Tag
from django.utils import timezone
from django.core.paginator import Paginator

POST_PER_PAGE=100


'''
Get all new posts in paginator.
'''
def get_new_posts():
    q=Post.objects.filter(status='ok',date__lte=timezone.now()).order_by('-id')
    return Paginator(q,POST_PER_PAGE)

'''
Get query in page.
n: page number
'''
def get_new_posts_page(n):
    return get_new_posts().page(n)

'''
Get all subscribed tag in paginator
tags: tag list
'''
def get_tags_posts(tags):
    q=Post.objects.filter(tags__in=tags).order_by('-id').distinct()
    return Paginator(q,POST_PER_PAGE)

'''
Get query in page.
n: page number
tags: tag list
'''
def get_tags_posts_page(n,tags):

    return get_tags_posts(n).page(tags)


'''
Get all subscribed tag in paginator
tag: tag 
'''
def get_tag_posts(tag):
    q=Post.objects.filter(status='ok',date__lte=timezone.now(),tags__in=[tag]).order_by('-date')
    return Paginator(q,POST_PER_PAGE)

'''
Get query in page.
n: page number
tags: tag list
'''

def get_tag_posts_page(n,tag):
    tag=Tag.objects.filter(title=n).first()
    if tag:
        return get_tag_posts(tag).page(1)
    else:
        return []


def get_page(req):
    p = req.GET.get('p')
    try:
        p = int(p)
    except:
        p = 1
    return p
