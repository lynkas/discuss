from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.
import uuid
import os
from django.utils.crypto import get_random_string
def ImagePath(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('image', filename)

class Post(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE,related_name="post_user")
    content = models.TextField(default="",blank=True)
    status = models.CharField(default="ok",max_length=10)
    '''
    status:
    ok          ok
    wait        waiting for releasing
    '''
    date = models.DateTimeField(auto_now=False,editable=True,default=timezone.now)
    tags = models.ManyToManyField("Tag",related_name="post_tag",blank=True)
    image = models.ManyToManyField("Image",related_name="post_image",blank=True)
    commentable = models.BooleanField(default=True)
    comments= models.ManyToManyField("Comment",blank=True,related_name='cmt')
    like=models.ManyToManyField(User,blank=True,related_name="post_like")
    likeCount = models.IntegerField(default=0)
    # like=models.ManyToManyField(User,related_name='liked')

class Tag(models.Model):
    title = models.CharField(max_length=20,unique=True)

class Image(models.Model):
    image = models.ImageField(upload_to=ImagePath)
    code = models.CharField(max_length=12,default=get_random_string)
    user = models.ForeignKey(User,on_delete=models.SET_NULL,null=True)

class Comment(models.Model):
    post = models.ForeignKey(Post,related_name='pst',on_delete=models.CASCADE,blank=True)
    user = models.ForeignKey(User,on_delete=models.SET_NULL,null=True,related_name="comment_user")
    content = models.TextField()
    date = models.DateTimeField(auto_now=False,editable=True,default=timezone.now)
    like=models.ManyToManyField(User,blank=True,related_name="comment_like")
    deleted = models.BooleanField(default=False)
    likeCount = models.IntegerField(default=0)
    # like = models.ManyToManyField(User, related_name='liked')

