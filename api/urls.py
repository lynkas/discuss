from .views import *
from django.urls import path
urlpatterns=[
    path('post/',post,name='post'),
    path('comment/',comment,name='comment'),
    path('upload/',image_upload,name='upload'),
    path('deleteimage/',image_delete,name='deleteimage'),
    path('delete/',delete_cp,name='delete'),
    path('like/',like,name='like'),
    path('getcomment/',get_comment,name='getcomment'),
]