from django.shortcuts import render,reverse
from django.http import HttpResponseForbidden,HttpResponseBadRequest,HttpResponse,HttpResponseRedirect,HttpResponseNotFound,HttpResponseNotAllowed
from datetime import datetime
import json
from post.models import Tag,Post,Image,Comment
from user.models import Notification
# Create your views here.

'''
Post a post

method: POST
content: content
tag: divided by space
timer:on/(Nothing)
    -if timer on:
        year(int)
        month(int)
        day(int)
        hour(int)
        minute(int)
uncommentable:on/(Nothing)

image: code of img, divided by dot.
      TFIgukgkf.hkjhLIHUI.HJkbhjjblkk.....

'''

BAD_REQUEST_METHOD="Wrong Request Method"
EMPTY_CONTENT = "Empty Content"
WRONG_TIME = "Wrong time format"

def error_message_makeup(msg):
    return json.dumps({'result':'error','mag':msg})

def ok_response(data=None):
    if data!=None:
        return HttpResponse(json.dumps({'result': 'ok','data':data}))
    return HttpResponse(json.dumps({'result':'ok'}))

def error_message_response(msg,status):
    if status==400:
        return HttpResponseBadRequest(error_message_makeup(msg))
    if status==403:
        return HttpResponseForbidden(error_message_makeup(msg))
    if status==404:
        return HttpResponseNotFound(error_message_makeup(msg))
    if status==405:
        return HttpResponseNotAllowed(error_message_makeup(msg))

def post(req):
    if not req.user.is_authenticated:
        return HttpResponseRedirect('/user/login/')
    if req.method!="POST":
        return error_message_response(BAD_REQUEST_METHOD,400)
    content=req.POST.get('content')
    tag=req.POST.get('tag')
    timer = req.POST.get('timer')
    uncommentable = req.POST.get('uncommentable')
    image = []
    imageCodes = req.FILES.get('image')
    if imageCodes:
        for i in imageCodes.split('.'):
            if i:
                img=Image.objects.filter(user=req.user,code=i).first()
                if img:
                    image.append(img)
    if not content and not image:
        return error_message_response(EMPTY_CONTENT, 400)


    if tag:
        tagList = tag.split()
    else:
        tagList=[]
    if timer:
        y=req.POST.get('year')
        m=req.POST.get('month')
        d=req.POST.get('day')
        h=req.POST.get('hour')
        min=req.POST.get('minute')
        if not all((y,m,d,h,min)):
            return error_message_response(WRONG_TIME,400)
        time=None
        try:
            y=int(y)
            m=int(m)
            d=int(d)
            h=int(h)
            min=int(min)
            time=datetime(y,m,d,h,min)
        except:
            return error_message_response(WRONG_TIME,400)
    else:
        time=None
    if uncommentable:
        commentable = False
    else:
        commentable = True
    tagObjectList=[]
    for tag in tagList:
        if len(tag)<20:
            tagObjectList.append(Tag.objects.get_or_create(title=tag)[0])

    if time:
        post=Post(user=req.user,
             content=content,
             status='wait',
             date=time,
             commentable=commentable,
             )

    else:
        post = Post(user=req.user,
                    content=content,
                    status='ok',
                    commentable=commentable,
                    )
    post.save()

    if image:
        post.images.add(*image)
    if tagList:
        post.tags.add(*tagList)

    return json.dumps({'result':'success'})

'''
POST
id postid
content content
'''
def comment(req):
    if not req.user.is_authenticated:
        return HttpResponseRedirect('/user/login/')
    if req.method != "post":
        return error_message_response(BAD_REQUEST_METHOD, 400)
    id=req.POST.get('id')
    post = Post.objects.filter(id=id).first()
    content = req.POST.get('id')

    if not post and content:
        return error_message_response('No such post', 404)
    comment=Comment(
        user=req.user,
        content=content,
        post=post
    )
    comment.save()
    Notification(user=post.user,action='pc',post=post).save()
    return ok_response()

def get_comment(req):
    id=req.GET.get('post')
    if not id:
        return error_message_response('Post is blank',400)
    post=Post.objects.filter(id=id).first()
    if not post:
        return error_message_response('No such post', 404)
    comments = post.comments.all().order_by('-likeCount')
    commentList = []
    for i in comments:
        commentList.append({
            'username':i.user.userext.nickName,
            'userprofile':i.user.userext.profile.url,
            'content':i.content,
            'like':i.likeCount
        })
    return json.dumps({'result':'ok','comments':commentList})



def image_upload(req):
    if not req.user.is_authenticated:
        return error_message_response('Need to login', 403)
    files=req.FILES.get('image')

    imageRes=[]
    if files:
        for i in req.FILES.getlist('image'):
            image=Image(image=i,user=req.user)
            image.save()
            imageRes.append({
                'url':image.image.url,
                'code':image.code
            })
    else:
        return error_message_response('No images',400)
    resp = {'result': 'success', 'image': imageRes}
    return HttpResponse(json.dumps(resp))


def image_delete(req):
    if not req.user.is_authenticated:
        return error_message_response('Need to login', 403)
    if req.method!='post':
        return error_message_response('Wrong method', 400)
    code = req.POST.get('code')
    query=Image.objects.filter(user=req.user,code=code)
    if len(query):
        query.delete()
        return {'result': 'success', 'code':code}
    else:
        return error_message_response('Image not found',404)

'''
GET
type post/comment
id id
operation like/unlike
'''
def like(req):
    if not req.user.is_authenticated:
        return HttpResponseRedirect('/user/login/')
    tp=req.GET.get('type')
    id=req.GET.get('id')
    op=req.GET.get('operation')
    if tp not in ['post','comment'] or not id or op not in ['like','unlike']:
        return error_message_response('Wrong para',400)

    if tp == "post":
        # lk =  Like.objects.filter(user=req.user,tp='post',post_id=id).first()
        obj=Post.objects.filter(id=id).first()
    else:
        # lk = Like.objects.filter(user=req.user,tp='comment',post_id=id,comment_id=id).first()
        obj = Comment.objects.filter(id=id).first()

    if not obj:
        return error_message_response('No Such Post',404)

    if op=='like':
        obj.like.add(req.user)
    elif op=='unlike':
        obj.like.remove(req.user)
    obj.save()
    t='pl' if tp=='post' else 'cl'
    if tp=='post':
        post=obj
        comment=None
    else:
        post=obj.post
        comment=obj

    a,_=Notification.objects.get_or_create(user=obj.user,action=t,post=post,comment=comment)
    a.save()

    return ok_response(len(obj.like.all()))
#
#
'''
GET
id id
type post/comment
'''

def delete_cp(req):
    if not req.user.is_authenticated:
        return HttpResponseRedirect('/user/login/')
    tp = req.GET.get('type')
    id = req.GET.get('id')
    if tp not in ['post','comment'] or not id:
        return error_message_response('Wrong para',400)
    if tp=='comment':
        obj=Comment.objects.filter(id=id,user=req.user).first()
    else:
        obj = Post.objects.filter(id=id, user=req.user).first()
    if not obj:
        return error_message_response('No such item',404)
    obj.delete()
    return ok_response()