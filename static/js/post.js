// $("#newButton").click(function () {
//     $("#demo").text('ok')
//     // $(this)
// })

$("#post-like").click(function() {
    const id=$(this).attr("pid");
    if ($(this).hasClass('liked')) {
        $.get({
        url:'/api/like/?type=post&id='+id+'&operation=unlike',
        success:(data) => {
            data = JSON.parse(data);
            

            $(this).attr('liked', 0);
            console.log("unliked.");
            $(this).removeClass('liked')
        }
    })
    } else {
        $.get({
        url:'/api/like/?type=post&id='+id+'&operation=like',
        success:(data) => {
            data = JSON.parse(data)
            
            $(this).parents("#post-head").find(".likeNum").text(data["data"])
            $(this).attr('liked', 1)
            console.log("liked.")
            $(this).addClass('liked')
        }
        })
    }
    // const likeNum = $(this).parents(".comment").find(".likeNum")
    // likeNum.text(parseInt(likeNum.text())+1)
})

$(".comment-like").click(function() {
    const id=$(this).attr('cid');
    if ($(this).attr('liked') === '1') {
        $.get({
        url:'/api/like/?type=comment&id='+id+'&operation=unlike',
        success:(data) => {
            data = JSON.parse(data)
            $(this).parents(".comment").find(".likeNum").text(parseInt(data["data"]))
            
            $(this).attr('liked', 0)
            $(this).removeClass('liked')
        }
    })
    } else {
        $.get({
        url:'/api/like/?type=comment&id='+id+'&operation=like',
        success:(data) => {
            data = JSON.parse(data)
            $(this).parents(".comment").find(".likeNum").text(parseInt(data["data"]))
            $(this).attr('liked', 1)
            
            $(this).addClass('liked')
        }
        })
    }
    // const likeNum = $(this).parents(".comment").find(".likeNum")
    // likeNum.text(parseInt(likeNum.text())+1)
    console.log("liked.")
})

$("#create").click(function() {
    const text = $("form")[0]
    text.submit()
})

$('#dp').click(function () {
    $.get({
        url:'/api/delete/?type=post&id='+$(this).attr('pid'),
        success:()=>{
            window.location='/'
        }
    })
})

$('.cp').click(function () {
    $.get({
        url:'/api/delete/?type=comment&id='+$(this).attr('cid'),
        success:()=>{
            window.location.reload()
        }
    })
})