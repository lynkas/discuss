from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Report(models.Model):
    user = models.ForeignKey(User,on_delete=models.SET_NULL,null=True,related_name="reported_by")

    objtype = models.CharField(max_length=10)
    '''
    objtype:
    post
    comment
    issue
    '''
    issue = models.TextField(blank=True, default="")
    date=models.DateTimeField(auto_now=True)
    post=models.ForeignKey("post.Post",on_delete=models.DO_NOTHING,blank=True,null=True)
    comment = models.ForeignKey("post.Comment",on_delete=models.DO_NOTHING,blank=True,null=True)
    processed = models.BooleanField(default=False)
    process_by = models.ForeignKey(User,on_delete=models.SET_NULL,null=True,blank=True,related_name="processed_by")