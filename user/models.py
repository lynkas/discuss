from django.db import models
from django.contrib.auth.models import User
# Create your models here.
import uuid
import django.utils.timezone as timezone
import os
from django.utils.crypto import get_random_string

def AvatarPath(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('avatar', filename)


class UserExt(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    profile = models.ImageField(default="image/anon.png",upload_to=AvatarPath)
    nickName = models.CharField(max_length=10)
    tags = models.ManyToManyField("post.Tag",blank=True)
    verified = models.BooleanField(default=False)


class Notification(models.Model):
    user = models.ForeignKey(User,related_name="notify_to",on_delete=models.CASCADE)
    action = models.CharField(max_length=10)
    '''
    action:
    cl - comment like
    pl - post like
    pc - post comment
    cc - comment comment
    '''
    post = models.ForeignKey("post.Post",on_delete=models.CASCADE,null=True,blank=True)
    comment = models.ForeignKey("post.Comment",on_delete=models.CASCADE,null=True,blank=True)
    count = models.IntegerField(default=1)

class Verification(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    vef = models.CharField(max_length=32,default=get_random_string)
    start = models.DateTimeField(default=timezone.now)
