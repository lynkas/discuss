from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse
from django.urls import reverse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate,login
from django import forms
from PIL import Image
from io import BytesIO
from user.models import Notification
from post.models import Post,Comment
from django.core.mail import send_mail
from api.views import error_message_response
from .models import UserExt,Verification
# Create your views here.

def Signup(request):

    if request.user.is_authenticated:
        return HttpResponseRedirect('/')
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            email = request.POST.get('email')
            if not email.lower().endswith('xjtlu.edu.cn'):
                return render(request, 'register.html', {'form': form})
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            UserExt(user=user,nickName="Jone Doe").save()
            # Verification(user=user).save()
            user.email=email
            user.save()
            a=Verification(user=user)
            a.save()
            # send_mail('Intrigued Email Verify','Please use the code '+a.vef+' finish the verification.','Intrigued@Intrigued.com',[email])

            return HttpResponseRedirect('/user/settings/')
        return render(request, 'register.html', {'form': form})
    else:
        form = UserCreationForm()
        return render(request, 'register.html', {'form': form})

def Login(req):
    if req.user.is_authenticated:
        return HttpResponseRedirect('/')
    if req.method=='POST':
        user=authenticate(username=req.POST.get('username'),password=req.POST.get('password'))
        if user is not None:
            login(req,user)
            return HttpResponseRedirect('/')
        else:
            return render(req, 'signin.html',{'result':'error','msg':'Wrong account/password pair'})

    else:
        return render(req, 'signin.html',{})

def personal(req):
    if not req.user.is_authenticated:
        return HttpResponseRedirect('/user/login/')
    post = Post.objects.filter(user=req.user).order_by('-id')
    comment = Comment.objects.filter(user=req.user)

    return render(req,"personal.html",context={'user':req.user,"posts":post,"comments":comment})

def cg(req):
    if not req.user.is_authenticated:
        return HttpResponseRedirect('/user/login/')
    ue=req.user.userext
    ue.nickName=req.GET.get('nick')
    ue.save()
    return HttpResponseRedirect('/user/')



def nf(req):
    if not req.user.is_authenticated:
        return HttpResponseRedirect('/user/login/')
    n=Notification.objects.filter(user=req.user).order_by('-id')
    return render(req,'notificationspage.html',context={'notification':n})

def pv(req):
    if not req.user.is_authenticated:
        return HttpResponseRedirect('/user/login/')
    vcode = req.GET.get('verified')
    if vcode:
        if Verification.objects.filter(user=req.user,vef=vcode).first():
            a=req.user.userext
            a.verified=True
            a.save()
    return HttpResponseRedirect('/user/')

def changea(req):
    if not req.user.is_authenticated:
        return HttpResponseRedirect('/user/login/')
    if req.method=="POST":
        pf=req.FILES.get('profile')
        if pf:
            ue=req.user.userext
            ue.profile=pf
            ue.save()
    return HttpResponseRedirect('/user/settings/')

class profile(forms.Form):
    image = forms.ImageField(required=False)
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())

def imgcrop(img,inpp):
    im=Image.open(img.file)
    output = BytesIO()
    im.crop((int(inpp['x']), int(inpp['y']), int(inpp['x'] + inpp['width']), int(inpp['y'] + inpp['height']))) \
        .resize((128, 128), Image.ANTIALIAS)\
        .save(output, quality=90,format=img.content_type.split('/')[-1])
    output.seek(0)
    return output

'''
POST

nickname: nickname
profile: profile(image)
    -if profile:
        x
        y
        height
        width
'''

def update_profile(req):
    if not req.user.is_authenticated:
        return HttpResponseRedirect('user/login/')
    if req.method=='GET':
        return render(req,'settingspage.html')
    else:
        msg=""
        ue=req.user.userext
        nickname=req.POST.get('nick')
        ue.nickName=nickname

        op=req.POST.get('op')
        np=req.POST.get('np')
        if op and np:
            user = authenticate(username=req.user.username, password=op)
            if user:
                user.set_password(np)
                user.save()
                login(req,user)
            else:
                msg='wrong password'
                return HttpResponse(render(req,'settingspage.html',context={'msg':msg}))
        ue.save()
        return HttpResponseRedirect('/')

def add_notification(user, action, obj):
    if action.startswith('c'):
        n,create=Notification.objects.get_or_create(user=user,action=action,comment=obj)
    else:
        n,create=Notification.objects.get_or_create(user=user, action=action, post=obj)

    if not create:
        n.count+=1
        n.save()

def remove_notifacition(user,obj):
    if type(obj)==Post:
        Notification.objects.filter(user=user,post=obj).delete()
    else:
        Notification.objects.filter(user=user, comment=obj).delete()
