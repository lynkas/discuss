from django.db import models
import uuid,os
# Create your models here.
def ImagePath(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('topic', filename)

class Topic(models.Model):
    t=models.CharField(max_length=30,default='')
    d=models.CharField(max_length=30,default='')
    pic = models.ImageField(upload_to=ImagePath)
    link = models.TextField(default="")